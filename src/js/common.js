// fixed svg show
//-----------------------------------------------------------------------------
svg4everybody();

// header nav
//-----------------------------------------------------------------------------------
(function () {
	var $nav = $('.js-mobile-nav');
	var $btn = $('.js-toggle-header-nav');

	$(window).on('scroll', function () {
		if (window.innerWidth < 1280) {
			hideNav();
		}
	});

	$btn.on('click', function () {
		toggleNav();
	});

	function toggleNav() {
		$btn.hasClass('open') ? hideNav() : showNav();
	}

	function showNav() {
		$btn.addClass('open');
		$nav.slideDown(200);
		$('body').addClass('scroll-disabled');
	}

	function hideNav() {
		$btn.removeClass('open');
		$nav.slideUp(200);
		$('body').removeClass('scroll-disabled');
	}
}());

const autoplayTime = 10000;

// sliders
//-----------------------------------------------------------------------------------
const mainSlider = function () {

	let mainSlider = new Swiper('.main-slider', {
		effect: 'fade',
		loop: true,
		autoplay: {
			delay: autoplayTime,
		},
		keyboard: {
			enabled: true,
		},
		navigation: {
			nextEl: '.js-slide-next',
			prevEl: '.js-slide-prev',
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,

		},
	});

};

const customerSlider = function () {



	let customerSlider = new Swiper('.customer-slider .swiper-container', {
		loop: true,
		slidesPerView: 5,
		autoplay: {
			delay: autoplayTime,
		},
		pagination: {
			el: '.customer-slider .pagination',
			type: 'fraction',
			renderFraction: function (currentClass, totalClass) {
				return '<span class="' + currentClass + '"></span>' +
					'/' +
					'<span class="' + totalClass + '"></span>';
			}
		},
		navigation: {
			nextEl: '.js-slide-next',
			prevEl: '.js-slide-prev',
		},
		keyboard: {
			enabled: true,
		},
		slidesPerView: 2,


		breakpoints: {
			// when window width is >= 768px
			400: {
				slidesPerView: 3,

			},

			[BREAKPOINTS.sm]: {
				slidesPerView: 3,

			},
			// when window width is >= 1280px
			[BREAKPOINTS.lg]: {
				slidesPerView: 5,

			}
		}

	});

};

const servicesSlider = function () {

	let servicesSlider = new Swiper('.services-slider .swiper-container', {
		loop: true,
		autoplay: {
			delay: autoplayTime,
		},
		navigation: {
			nextEl: '.js-slide-next',
			prevEl: '.js-slide-prev',
		},
		pagination: {
			el: '.services-slider .pagination',
			type: 'fraction',
			renderFraction: function (currentClass, totalClass) {
				return '<span class="' + currentClass + '"></span>' +
					'/' +
					'<span class="' + totalClass + '"></span>';
			}
		},
		speed: 600,
		parallax: true,
		keyboard: {
			enabled: true,
		},


		breakpoints: {
			// when window width is >= 768px
			[BREAKPOINTS.md]: {
				pagination: false,
			},
			
		}

	});

};


const modals = function () {
	const buttonAttr = 'modal-trigger';
	const modalAttr = 'modal-id';
	const activeClassModal = 'modal--is-opening';
	const container = $('.modal-container');
	const activeClassContainer = 'modal-container--is-visible';
	const button = $(`[${buttonAttr}]`);
	const closeButton = $('.js-close-modal');
	const closeAllModal = $('.js-close-all-modals');
	const toggle = (id) => {
		const modal = $(`[${modalAttr}='${id}']`);

		let isOpening = container.hasClass(activeClassContainer);

		if (!isOpening) {
			container.addClass(activeClassContainer);
			modal.addClass(activeClassModal);
		} else {
			container.removeClass(activeClassContainer);
			modal.removeClass(activeClassModal);
		}


	}

	button.click(function () {
		const id = $(this).attr(buttonAttr);

		toggle(id);
	});

	closeButton.click(function () {
		const id = $(this).closest(`[${modalAttr}]`).attr(modalAttr);

		toggle(id);
	});

	closeAllModal.click(function () {
		$(`[${modalAttr}]`).removeClass(activeClassModal);
		container.removeClass(activeClassContainer);
	})
}


$(document).ready(function () {
	mainSlider();
	customerSlider();
	servicesSlider();
	modals();
});