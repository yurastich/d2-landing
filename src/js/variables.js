'use strict';

var BREAKPOINTS = {
  lg: 1280,
  md: 1010,
  sm: 768,
  xs: 320
};
