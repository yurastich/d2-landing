'use strict';

var BREAKPOINTS = {
  lg: 1280,
  md: 1010,
  sm: 768,
  xs: 320
};

function formClear($form) {
	if ($form.prop("tagName") == 'FORM') {
		$form.get(0).reset();
		$form.validate().resetForm();
	} else {
		$form.find('input, textarea').each(function(){
			var $_field = $(this);

			$_field.val('')
						 .removeClass('error')
						 .next('.error').remove();
		});
	}
};

function isFormValid($form) {
	var isValid = true;

	if ($form.prop("tagName") == 'FORM') {
		isValid = $form.validate().checkForm();
	} else {
		$form.find('input, textarea').each(function(){
			var val = $(this).closest('form').validate();

			if (!val.element($(this))) {
				isValid = false;
				return false;
			}
		});
	}

	return isValid;
};

function getCurrentBreakpoint() {
	var currentPoint;

	for (var key in BREAKPOINTS) {
		if (breakpoints[key] <= window.innerWidth) {
			currentPoint = key;
			return currentPoint;
		}
	}
};

function changeDataValidError($input, isValid){
	if (isValid) {
		$input.removeAttr('data-valid-error');
	} else {
		$input.attr('data-valid-error', 'error');
	}

	$input.valid();
};

function scrollToEl($el, time) {
	var time = time === undefined ? time : 350;
  $('html, body').animate({
      scrollTop: $el.offset().top
  }, time);
};

// fixed svg show
//-----------------------------------------------------------------------------
svg4everybody();

// header nav
//-----------------------------------------------------------------------------------
(function () {
	var $nav = $('.js-mobile-nav');
	var $btn = $('.js-toggle-header-nav');

	$(window).on('scroll', function () {
		if (window.innerWidth < 1280) {
			hideNav();
		}
	});

	$btn.on('click', function () {
		toggleNav();
	});

	function toggleNav() {
		$btn.hasClass('open') ? hideNav() : showNav();
	}

	function showNav() {
		$btn.addClass('open');
		$nav.slideDown(200);
		$('body').addClass('scroll-disabled');
	}

	function hideNav() {
		$btn.removeClass('open');
		$nav.slideUp(200);
		$('body').removeClass('scroll-disabled');
	}
}());

const autoplayTime = 10000;

// sliders
//-----------------------------------------------------------------------------------
const mainSlider = function () {

	let mainSlider = new Swiper('.main-slider', {
		effect: 'fade',
		loop: true,
		autoplay: {
			delay: autoplayTime,
		},
		keyboard: {
			enabled: true,
		},
		navigation: {
			nextEl: '.js-slide-next',
			prevEl: '.js-slide-prev',
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,

		},
	});

};

const customerSlider = function () {



	let customerSlider = new Swiper('.customer-slider .swiper-container', {
		loop: true,
		slidesPerView: 5,
		autoplay: {
			delay: autoplayTime,
		},
		pagination: {
			el: '.customer-slider .pagination',
			type: 'fraction',
			renderFraction: function (currentClass, totalClass) {
				return '<span class="' + currentClass + '"></span>' +
					'/' +
					'<span class="' + totalClass + '"></span>';
			}
		},
		navigation: {
			nextEl: '.js-slide-next',
			prevEl: '.js-slide-prev',
		},
		keyboard: {
			enabled: true,
		},
		slidesPerView: 2,


		breakpoints: {
			// when window width is >= 768px
			400: {
				slidesPerView: 3,

			},

			[BREAKPOINTS.sm]: {
				slidesPerView: 3,

			},
			// when window width is >= 1280px
			[BREAKPOINTS.lg]: {
				slidesPerView: 5,

			}
		}

	});

};

const servicesSlider = function () {

	let servicesSlider = new Swiper('.services-slider .swiper-container', {
		loop: true,
		autoplay: {
			delay: autoplayTime,
		},
		navigation: {
			nextEl: '.js-slide-next',
			prevEl: '.js-slide-prev',
		},
		pagination: {
			el: '.services-slider .pagination',
			type: 'fraction',
			renderFraction: function (currentClass, totalClass) {
				return '<span class="' + currentClass + '"></span>' +
					'/' +
					'<span class="' + totalClass + '"></span>';
			}
		},
		speed: 600,
		parallax: true,
		keyboard: {
			enabled: true,
		},


		breakpoints: {
			// when window width is >= 768px
			[BREAKPOINTS.md]: {
				pagination: false,
			},
			
		}

	});

};


const modals = function () {
	const buttonAttr = 'modal-trigger';
	const modalAttr = 'modal-id';
	const activeClassModal = 'modal--is-opening';
	const container = $('.modal-container');
	const activeClassContainer = 'modal-container--is-visible';
	const button = $(`[${buttonAttr}]`);
	const closeButton = $('.js-close-modal');
	const closeAllModal = $('.js-close-all-modals');
	const toggle = (id) => {
		const modal = $(`[${modalAttr}='${id}']`);

		let isOpening = container.hasClass(activeClassContainer);

		if (!isOpening) {
			container.addClass(activeClassContainer);
			modal.addClass(activeClassModal);
		} else {
			container.removeClass(activeClassContainer);
			modal.removeClass(activeClassModal);
		}


	}

	button.click(function () {
		const id = $(this).attr(buttonAttr);

		toggle(id);
	});

	closeButton.click(function () {
		const id = $(this).closest(`[${modalAttr}]`).attr(modalAttr);

		toggle(id);
	});

	closeAllModal.click(function () {
		$(`[${modalAttr}]`).removeClass(activeClassModal);
		container.removeClass(activeClassContainer);
	})
}


$(document).ready(function () {
	mainSlider();
	customerSlider();
	servicesSlider();
	modals();
});
// custom jQuery validation
// add to validate form class 'js-validate'
// add to fields attr 'name'
//-----------------------------------------------------------------------------------
var validator = {
  init: function () {
    $('form').each(function () {
      var $form = $(this);
      var config = {
        errorElement : 'b',
        errorClass   : 'error',
        focusInvalid : false,
        focusCleanup : true,
        errorPlacement: function (error, element) {
          validator.setError($(element), error);
        },
        highlight: function (element, errorClass, validClass) {
          var $el     = validator.defineElement($(element));
          var $elWrap = $el.closest('.el-form-field');

          if ($el) $el.removeClass(validClass).addClass(errorClass);
          if ($elWrap.length) $elWrap.removeClass(validClass).addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
          var $el     = validator.defineElement($(element));
          var $elWrap = $el.closest('.el-form-field');

          if ($el) $el.removeClass(errorClass).addClass(validClass);
          if ($elWrap.length) $elWrap.removeClass(errorClass).addClass(validClass);
        }
      };
      if ($form.hasClass('js-validate')) {
        $form.validate(config);
      }
    });
  },
  setError: function ($el, message) {
    $el = this.defineElement($el);
    if ($el) this.domWorker.error($el, message);
  },
  defineElement: function ($el) {
    return $el;
  },
  domWorker: {
    error: function ($el, message) {
      var $elWrap = $el.closest('.el-form-field');
      $el.addClass('error');
      if ($elWrap.length) $elWrap.addClass('error');
      $el.after(message);
    }
  }
};

validator.init();

// validate by data attribute
//-----------------------------------------------------------------------------------
(function(){
  // add to validate field data-valid="test"
  //-----------------------------------------------------------------------------------
  var rules = {
    'user_password': {
      minlength: 6
    },
    'user_password_repeat': {
      minlength: 6,
      equalTo: '#user_password',
      messages: {
        equalTo: "Passwords doesn't same"
      }
    }
  };

  for (var ruleName in rules) {
    $('[data-valid=' + ruleName + ']').each(function(){
      $(this).rules('add', rules[ruleName]);
    });
  };
}());

// global messages
//-----------------------------------------------------------------------------------
$.validator.messages.minlength = 'Не менее {0} символов';
$.validator.messages.required = 'Это поле обязательное';

// custom rules
//-----------------------------------------------------------------------------------
$.validator.addMethod("email", function(value) {
  if (value == '') return true;
  var regexp = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  return regexp.test(value);
});

$.validator.addMethod("letters", function(value, element) {
  return this.optional(element) || /^[^1-9!@#\$%\^&\*\(\)\[\]:;,.?=+_<>`~\\\/"]+$/i.test(value);
});

$.validator.addMethod("digits", function(value, element) {
  return this.optional(element) || /^(\+?\d+)?\s*(\(\d+\))?[\s-]*([\d-]*)$/i.test(value);
});

$.validator.addMethod('dataValidError', function(value, element) {
  var $el = validator.defineElement($(element));

  return this.optional(element) || !$el.attr('data-valid-error');
});
